var class_assets_1_1_scripts_1_1_items_1_1_weapon =
[
    [ "EquipWeapon", "class_assets_1_1_scripts_1_1_items_1_1_weapon.html#af90d2d1ed8c275c0c831998635b40509", null ],
    [ "UnequipWeapon", "class_assets_1_1_scripts_1_1_items_1_1_weapon.html#afa1e773b2651647094e3ee9acd0237d5", null ],
    [ "attackPowerBonus", "class_assets_1_1_scripts_1_1_items_1_1_weapon.html#a3f2a62662cac8e5ffdd98327fa49cef9", null ],
    [ "attackSpeedBonus", "class_assets_1_1_scripts_1_1_items_1_1_weapon.html#a444bf6ab16b9a4313839afa299cd06b4", null ],
    [ "AttackPowerBonus", "class_assets_1_1_scripts_1_1_items_1_1_weapon.html#ae527d7768e715e2a15b939731a1cf122", null ],
    [ "AttackSpeedBonus", "class_assets_1_1_scripts_1_1_items_1_1_weapon.html#a103f9c8920c5a1c56909e084f047588d", null ],
    [ "GearSlot", "class_assets_1_1_scripts_1_1_items_1_1_weapon.html#af8b4f9e142662e582547c2ce26db8fef", null ]
];