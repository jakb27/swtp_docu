var _attribute_conversion_rates_8cs =
[
    [ "AttributeConversionRates", "_attribute_conversion_rates_8cs.html#aec4a1829ecde34a782a8e78a967d1327", [
      [ "StrengthToAttackPower", "_attribute_conversion_rates_8cs.html#aec4a1829ecde34a782a8e78a967d1327abab4e096adbb2a2f8cee5ad5b9d3ef20", null ],
      [ "AgilityToAttackSpeed", "_attribute_conversion_rates_8cs.html#aec4a1829ecde34a782a8e78a967d1327a7d907d221899e10648c7812724302182", null ],
      [ "AgilityToMovementSpeed", "_attribute_conversion_rates_8cs.html#aec4a1829ecde34a782a8e78a967d1327afea01cd5bc92091fc2dfd376f6e94e75", null ],
      [ "IntellectToMana", "_attribute_conversion_rates_8cs.html#aec4a1829ecde34a782a8e78a967d1327a592afa82f217826dadecd0f61463af26", null ],
      [ "IntellectToSpellPower", "_attribute_conversion_rates_8cs.html#aec4a1829ecde34a782a8e78a967d1327a44f03b910a732165536e1c54f7722d76", null ],
      [ "StaminaToHealth", "_attribute_conversion_rates_8cs.html#aec4a1829ecde34a782a8e78a967d1327aab812ed2ae22e75087a737a703a41743", null ],
      [ "StaminaToArmor", "_attribute_conversion_rates_8cs.html#aec4a1829ecde34a782a8e78a967d1327aad36c2f4ac72358d14258e7b930f16d1", null ]
    ] ]
];