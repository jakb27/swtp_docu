var dir_42ab1ac0264620c4dc4867e164e20f5e =
[
    [ "Armor.cs", "_armor_8cs.html", [
      [ "Armor", "class_assets_1_1_scripts_1_1_items_1_1_armor.html", "class_assets_1_1_scripts_1_1_items_1_1_armor" ]
    ] ],
    [ "Currency.cs", "_currency_8cs.html", [
      [ "Currency", "class_assets_1_1_scripts_1_1_items_1_1_currency.html", "class_assets_1_1_scripts_1_1_items_1_1_currency" ]
    ] ],
    [ "Gear.cs", "_gear_8cs.html", [
      [ "Gear", "class_assets_1_1_scripts_1_1_items_1_1_gear.html", "class_assets_1_1_scripts_1_1_items_1_1_gear" ]
    ] ],
    [ "GearGenerator.cs", "_gear_generator_8cs.html", [
      [ "GearGenerator", "class_assets_1_1_scripts_1_1_items_1_1_gear_generator.html", "class_assets_1_1_scripts_1_1_items_1_1_gear_generator" ]
    ] ],
    [ "GearInfos.cs", "_gear_infos_8cs.html", [
      [ "GearInfos", "class_assets_1_1_scripts_1_1_items_1_1_gear_infos.html", "class_assets_1_1_scripts_1_1_items_1_1_gear_infos" ]
    ] ],
    [ "GearRarity.cs", "_gear_rarity_8cs.html", "_gear_rarity_8cs" ],
    [ "GearSlot.cs", "_gear_slot_8cs.html", "_gear_slot_8cs" ],
    [ "Item.cs", "_item_8cs.html", [
      [ "Item", "class_assets_1_1_scripts_1_1_items_1_1_item.html", "class_assets_1_1_scripts_1_1_items_1_1_item" ]
    ] ],
    [ "LootTable.cs", "_loot_table_8cs.html", [
      [ "LootTable", "class_assets_1_1_scripts_1_1_items_1_1_loot_table.html", "class_assets_1_1_scripts_1_1_items_1_1_loot_table" ],
      [ "Loot", "class_assets_1_1_scripts_1_1_items_1_1_loot_table_1_1_loot.html", "class_assets_1_1_scripts_1_1_items_1_1_loot_table_1_1_loot" ]
    ] ],
    [ "Potion.cs", "_potion_8cs.html", [
      [ "Potion", "class_assets_1_1_scripts_1_1_items_1_1_potion.html", "class_assets_1_1_scripts_1_1_items_1_1_potion" ]
    ] ],
    [ "PotionType.cs", "_potion_type_8cs.html", "_potion_type_8cs" ],
    [ "UseableItem.cs", "_useable_item_8cs.html", [
      [ "UseableItem", "class_assets_1_1_scripts_1_1_items_1_1_useable_item.html", "class_assets_1_1_scripts_1_1_items_1_1_useable_item" ]
    ] ],
    [ "Weapon.cs", "_weapon_8cs.html", [
      [ "Weapon", "class_assets_1_1_scripts_1_1_items_1_1_weapon.html", "class_assets_1_1_scripts_1_1_items_1_1_weapon" ]
    ] ],
    [ "WorldItem.cs", "_world_item_8cs.html", [
      [ "WorldItem", "class_assets_1_1_scripts_1_1_items_1_1_world_item.html", "class_assets_1_1_scripts_1_1_items_1_1_world_item" ]
    ] ]
];