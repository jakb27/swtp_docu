var class_assets_1_1_scripts_1_1_controller_1_1_game_manager =
[
    [ "GameOver", "class_assets_1_1_scripts_1_1_controller_1_1_game_manager.html#ae27f001dd04ebacb253f660f5e55afd7", null ],
    [ "GameWon", "class_assets_1_1_scripts_1_1_controller_1_1_game_manager.html#a2d24b62ccdf6abe7402efd4846664b66", null ],
    [ "LoadGameDifficulty", "class_assets_1_1_scripts_1_1_controller_1_1_game_manager.html#a1270fbd2c0a884fcb259bece9b3efaac", null ],
    [ "PauseGame", "class_assets_1_1_scripts_1_1_controller_1_1_game_manager.html#a782a70c3ca2f7e5ee6db1d094490c808", null ],
    [ "ResetGameDifficulty", "class_assets_1_1_scripts_1_1_controller_1_1_game_manager.html#aec4705ac8641b66c161ac97f6a3b5e03", null ],
    [ "ResumeGame", "class_assets_1_1_scripts_1_1_controller_1_1_game_manager.html#a4d6bb1de01913a172f254cdc2ae1fcd4", null ],
    [ "SaveGameDifficulty", "class_assets_1_1_scripts_1_1_controller_1_1_game_manager.html#a55a3d6e5c0ab73cf02d757f10c285fc2", null ],
    [ "SetMenuReady", "class_assets_1_1_scripts_1_1_controller_1_1_game_manager.html#a2823e0d31e21e9657cc40ea852aa1315", null ],
    [ "MenuReady", "class_assets_1_1_scripts_1_1_controller_1_1_game_manager.html#ae322425f6276eb282cbe8b78e6da7d81", null ],
    [ "CurrentGameState", "class_assets_1_1_scripts_1_1_controller_1_1_game_manager.html#a45d226ba618de0679cdfcb19c0882489", null ],
    [ "GameDifficulty", "class_assets_1_1_scripts_1_1_controller_1_1_game_manager.html#aa3c5086c34f5b494fdf33f46cf086614", null ]
];