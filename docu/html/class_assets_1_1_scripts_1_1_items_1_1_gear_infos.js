var class_assets_1_1_scripts_1_1_items_1_1_gear_infos =
[
    [ "ARMOR_BONUS_ATTRIBUTE_MAX_VALUE_EPIC", "class_assets_1_1_scripts_1_1_items_1_1_gear_infos.html#af37664418fd371f14c0661c511af0eae", null ],
    [ "ARMOR_BONUS_ATTRIBUTE_MAX_VALUE_LEGENDARY", "class_assets_1_1_scripts_1_1_items_1_1_gear_infos.html#a1d9af73d14a0c0e2bd9381ac404f56a7", null ],
    [ "ARMOR_BONUS_ATTRIBUTE_MIN_VALUE_EPIC", "class_assets_1_1_scripts_1_1_items_1_1_gear_infos.html#a62e841d8b41b5fd2292a2bb8a493414b", null ],
    [ "ARMOR_BONUS_ATTRIBUTE_MIN_VALUE_LEGENDARY", "class_assets_1_1_scripts_1_1_items_1_1_gear_infos.html#aedd58eaf67bdedf897aeb558c8fb15b5", null ],
    [ "GEAR_ATTRIBUTE_MAX_VALUE_LEGENDARY", "class_assets_1_1_scripts_1_1_items_1_1_gear_infos.html#a1dd55956833473f17e681209b6ff8eab", null ],
    [ "GEAR_ATTRIBUTE_MIN_VALUE_EPIC", "class_assets_1_1_scripts_1_1_items_1_1_gear_infos.html#ac026607cccd9e2e8865644bb1d945860", null ],
    [ "GEAR_ATTRIBUTE_MIN_VALUE_LEGENDARY", "class_assets_1_1_scripts_1_1_items_1_1_gear_infos.html#af58ca3e7449cc4a0073e33a995e096ad", null ],
    [ "GEAR_ATTRIBUTE_MIN_VALUE_RARE", "class_assets_1_1_scripts_1_1_items_1_1_gear_infos.html#af5ed2b149f698d55ebb833586f129564", null ],
    [ "GEAR_ATTRIBUTE_MIN_VALUE_UNCOMMON", "class_assets_1_1_scripts_1_1_items_1_1_gear_infos.html#a0c6e2e18c7a5d60aafe92f40071aa776", null ],
    [ "GEAR_CHANCE_RARITY_EPIC", "class_assets_1_1_scripts_1_1_items_1_1_gear_infos.html#aef26d55d912b85d6c0a59fe2e2458882", null ],
    [ "GEAR_CHANCE_RARITY_LEGENDARY", "class_assets_1_1_scripts_1_1_items_1_1_gear_infos.html#a2ff55d6bfced8bacbe848ce105aeb3c0", null ],
    [ "GEAR_CHANCE_RARITY_RARE", "class_assets_1_1_scripts_1_1_items_1_1_gear_infos.html#adab234d0f6049d182fd58b89329b8aad", null ],
    [ "GEAR_RARITY_BREAKPOINT_EPIC", "class_assets_1_1_scripts_1_1_items_1_1_gear_infos.html#a1de2f652f2b776ff475253bc451f4395", null ],
    [ "GEAR_RARITY_BREAKPOINT_LEGENDARY", "class_assets_1_1_scripts_1_1_items_1_1_gear_infos.html#a0eb5a9bceb81f5db1705572eff0e3c02", null ],
    [ "GEAR_RARITY_BREAKPOINT_RARE", "class_assets_1_1_scripts_1_1_items_1_1_gear_infos.html#a625adbec5489ad693eea7127f64c2ed9", null ],
    [ "RING_BONUS_ATTRIBUTE_MAX_VALUE_EPIC", "class_assets_1_1_scripts_1_1_items_1_1_gear_infos.html#a25681c71de73758e9a179624efb51fc9", null ],
    [ "RING_BONUS_ATTRIBUTE_MAX_VALUE_LEGENDARY", "class_assets_1_1_scripts_1_1_items_1_1_gear_infos.html#ae441b970d12fa2e03fbfdf0f526f469d", null ],
    [ "RING_BONUS_ATTRIBUTE_MIN_VALUE_EPIC", "class_assets_1_1_scripts_1_1_items_1_1_gear_infos.html#acf6d75f1911f4e7e4041dbe883d0a927", null ],
    [ "RING_BONUS_ATTRIBUTE_MIN_VALUE_LEGENDARY", "class_assets_1_1_scripts_1_1_items_1_1_gear_infos.html#aff5c5dda69893648b2dd245256dc0a75", null ],
    [ "RING_BONUS_ATTRIBUTE_MIN_VALUE_RARE", "class_assets_1_1_scripts_1_1_items_1_1_gear_infos.html#aaadbb4baa3d55069d59a83fa0b4bcbb9", null ],
    [ "RING_BONUS_ATTRIBUTE_MIN_VALUE_UNCOMMON", "class_assets_1_1_scripts_1_1_items_1_1_gear_infos.html#a1592e69c9fc5d69e90fdff821f4f7924", null ],
    [ "WEAPON_BONUS_ATTRIBUTE_MAX_VALUE_EPIC", "class_assets_1_1_scripts_1_1_items_1_1_gear_infos.html#a5d9289b55b28abc71ac2d3e9a4b3edda", null ],
    [ "WEAPON_BONUS_ATTRIBUTE_MAX_VALUE_LEGENDARY", "class_assets_1_1_scripts_1_1_items_1_1_gear_infos.html#a0b493bb83a13cabb4cc1ec07c4d1fd26", null ],
    [ "WEAPON_BONUS_ATTRIBUTE_MIN_VALUE_EPIC", "class_assets_1_1_scripts_1_1_items_1_1_gear_infos.html#a573e1137f7fdb307e32bdf21894f4bd6", null ],
    [ "WEAPON_BONUS_ATTRIBUTE_MIN_VALUE_LEGENDARY", "class_assets_1_1_scripts_1_1_items_1_1_gear_infos.html#a79d4e95ec3d7fac19e235514e6571125", null ]
];