var hierarchy =
[
    [ "Assets.Scripts.Abilities.AbilityCooldowns", "class_assets_1_1_scripts_1_1_abilities_1_1_ability_cooldowns.html", null ],
    [ "Assets.Scripts.Abilities.AbilityCosts", "class_assets_1_1_scripts_1_1_abilities_1_1_ability_costs.html", null ],
    [ "Assets.Scripts.Attributes.BaseBonus", "class_assets_1_1_scripts_1_1_attributes_1_1_base_bonus.html", [
      [ "Assets.Scripts.Attributes.FinalBonus", "class_assets_1_1_scripts_1_1_attributes_1_1_final_bonus.html", null ],
      [ "Assets.Scripts.Attributes.RawBonus", "class_assets_1_1_scripts_1_1_attributes_1_1_raw_bonus.html", null ]
    ] ],
    [ "Assets.Scripts.Utility.JsonSerializer.DataSerializer", "class_assets_1_1_scripts_1_1_utility_1_1_json_serializer_1_1_data_serializer.html", null ],
    [ "Assets.Scripts.Enemies.EnemyInfos", "class_assets_1_1_scripts_1_1_enemies_1_1_enemy_infos.html", null ],
    [ "Assets.Scripts.UI.Inventory.EquipmentInfos", "class_assets_1_1_scripts_1_1_u_i_1_1_inventory_1_1_equipment_infos.html", null ],
    [ "Assets.Scripts.Items.GearInfos", "class_assets_1_1_scripts_1_1_items_1_1_gear_infos.html", null ],
    [ "Assets.Scripts.Utility.Globals", "class_assets_1_1_scripts_1_1_utility_1_1_globals.html", null ],
    [ "IBeginDragHandler", null, [
      [ "Assets.Scripts.UI.Inventory.DragDropHandler", "class_assets_1_1_scripts_1_1_u_i_1_1_inventory_1_1_drag_drop_handler.html", null ]
    ] ],
    [ "IDragHandler", null, [
      [ "Assets.Scripts.UI.Inventory.DragDropHandler", "class_assets_1_1_scripts_1_1_u_i_1_1_inventory_1_1_drag_drop_handler.html", null ]
    ] ],
    [ "IDropHandler", null, [
      [ "Assets.Scripts.UI.Inventory.DragDropHandler", "class_assets_1_1_scripts_1_1_u_i_1_1_inventory_1_1_drag_drop_handler.html", null ]
    ] ],
    [ "IEndDragHandler", null, [
      [ "Assets.Scripts.UI.Inventory.DragDropHandler", "class_assets_1_1_scripts_1_1_u_i_1_1_inventory_1_1_drag_drop_handler.html", null ]
    ] ],
    [ "IPointerClickHandler", null, [
      [ "Assets.Scripts.UI.Inventory.DragDropHandler", "class_assets_1_1_scripts_1_1_u_i_1_1_inventory_1_1_drag_drop_handler.html", null ]
    ] ],
    [ "Assets.Scripts.Utility.JsonSerializer.ISerializedObject", "interface_assets_1_1_scripts_1_1_utility_1_1_json_serializer_1_1_i_serialized_object.html", [
      [ "Assets.Scripts.Utility.JsonSerializer.GameDifficulty", "class_assets_1_1_scripts_1_1_utility_1_1_json_serializer_1_1_game_difficulty.html", null ],
      [ "Assets.Scripts.Utility.JsonSerializer.ItemNames", "class_assets_1_1_scripts_1_1_utility_1_1_json_serializer_1_1_item_names.html", null ],
      [ "Assets.Scripts.Utility.JsonSerializer.StatDisplayInitialization", "class_assets_1_1_scripts_1_1_utility_1_1_json_serializer_1_1_stat_display_initialization.html", null ]
    ] ],
    [ "Assets.Scripts.Utility.JsonSerializer.JsonFileNames", "class_assets_1_1_scripts_1_1_utility_1_1_json_serializer_1_1_json_file_names.html", null ],
    [ "Assets.Scripts.LevelGeneration.LevelRoom", "class_assets_1_1_scripts_1_1_level_generation_1_1_level_room.html", null ],
    [ "Assets.Scripts.Utility.LoggingService", "class_assets_1_1_scripts_1_1_utility_1_1_logging_service.html", null ],
    [ "Assets.Scripts.Items.LootTable.Loot", "class_assets_1_1_scripts_1_1_items_1_1_loot_table_1_1_loot.html", null ],
    [ "MonoBehaviour", null, [
      [ "Assets.Scripts.Abilities.AttackBullet", "class_assets_1_1_scripts_1_1_abilities_1_1_attack_bullet.html", null ],
      [ "Assets.Scripts.Abilities.BaseAbility", "class_assets_1_1_scripts_1_1_abilities_1_1_base_ability.html", [
        [ "Assets.Scripts.Abilities.AbilityDash", "class_assets_1_1_scripts_1_1_abilities_1_1_ability_dash.html", null ],
        [ "Assets.Scripts.Abilities.AbilityHeal", "class_assets_1_1_scripts_1_1_abilities_1_1_ability_heal.html", null ],
        [ "Assets.Scripts.Abilities.AbilityShootBullet", "class_assets_1_1_scripts_1_1_abilities_1_1_ability_shoot_bullet.html", null ],
        [ "Assets.Scripts.Abilities.AbilitySpeedBuff", "class_assets_1_1_scripts_1_1_abilities_1_1_ability_speed_buff.html", null ]
      ] ],
      [ "Assets.Scripts.Attributes.Attribute", "class_assets_1_1_scripts_1_1_attributes_1_1_attribute.html", [
        [ "Assets.Scripts.Attributes.PrimaryAttribute", "class_assets_1_1_scripts_1_1_attributes_1_1_primary_attribute.html", null ],
        [ "Assets.Scripts.Attributes.SecondaryAttribute", "class_assets_1_1_scripts_1_1_attributes_1_1_secondary_attribute.html", null ]
      ] ],
      [ "Assets.Scripts.Enemies.Enemy", "class_assets_1_1_scripts_1_1_enemies_1_1_enemy.html", [
        [ "Assets.Scripts.Enemies.BasicEnemy", "class_assets_1_1_scripts_1_1_enemies_1_1_basic_enemy.html", null ],
        [ "Assets.Scripts.Enemies.FinalBoss", "class_assets_1_1_scripts_1_1_enemies_1_1_final_boss.html", null ]
      ] ],
      [ "Assets.Scripts.Enemies.EnemyMovement", "class_assets_1_1_scripts_1_1_enemies_1_1_enemy_movement.html", null ],
      [ "Assets.Scripts.Enemies.FieldOfView", "class_assets_1_1_scripts_1_1_enemies_1_1_field_of_view.html", null ],
      [ "Assets.Scripts.Items.LootTable", "class_assets_1_1_scripts_1_1_items_1_1_loot_table.html", null ],
      [ "Assets.Scripts.Items.WorldItem", "class_assets_1_1_scripts_1_1_items_1_1_world_item.html", null ],
      [ "Assets.Scripts.LevelGeneration.Level", "class_assets_1_1_scripts_1_1_level_generation_1_1_level.html", null ],
      [ "Assets.Scripts.LevelGeneration.Room", "class_assets_1_1_scripts_1_1_level_generation_1_1_room.html", [
        [ "Assets.Scripts.LevelGeneration.StartRoom", "class_assets_1_1_scripts_1_1_level_generation_1_1_start_room.html", null ]
      ] ],
      [ "Assets.Scripts.UI.Inventory.DragDropHandler", "class_assets_1_1_scripts_1_1_u_i_1_1_inventory_1_1_drag_drop_handler.html", null ],
      [ "Assets.Scripts.UI.Inventory.Equipment", "class_assets_1_1_scripts_1_1_u_i_1_1_inventory_1_1_equipment.html", null ],
      [ "Assets.Scripts.UI.Inventory.EquipmentSlot", "class_assets_1_1_scripts_1_1_u_i_1_1_inventory_1_1_equipment_slot.html", null ],
      [ "Assets.Scripts.UI.Inventory.InventoryButtonController", "class_assets_1_1_scripts_1_1_u_i_1_1_inventory_1_1_inventory_button_controller.html", null ],
      [ "Assets.Scripts.UI.Inventory.InventorySlot", "class_assets_1_1_scripts_1_1_u_i_1_1_inventory_1_1_inventory_slot.html", null ],
      [ "Assets.Scripts.UI.Menus.IngameMenu", "class_assets_1_1_scripts_1_1_u_i_1_1_menus_1_1_ingame_menu.html", [
        [ "Assets.Scripts.UI.Menus.GameOverMenu", "class_assets_1_1_scripts_1_1_u_i_1_1_menus_1_1_game_over_menu.html", null ],
        [ "Assets.Scripts.UI.Menus.GameWonMenu", "class_assets_1_1_scripts_1_1_u_i_1_1_menus_1_1_game_won_menu.html", null ],
        [ "Assets.Scripts.UI.Menus.PauseMenu", "class_assets_1_1_scripts_1_1_u_i_1_1_menus_1_1_pause_menu.html", null ]
      ] ],
      [ "Assets.Scripts.UI.Menus.MainMenu", "class_assets_1_1_scripts_1_1_u_i_1_1_menus_1_1_main_menu.html", null ],
      [ "Assets.Scripts.UI.Menus.StatDisplayElement", "class_assets_1_1_scripts_1_1_u_i_1_1_menus_1_1_stat_display_element.html", null ]
    ] ],
    [ "Assets.Scripts.Player.PlayerInfos", "class_assets_1_1_scripts_1_1_player_1_1_player_infos.html", null ],
    [ "Assets.Scripts.LevelGeneration.RoomInfos", "class_assets_1_1_scripts_1_1_level_generation_1_1_room_infos.html", null ],
    [ "Assets.Scripts.UI.Menus.Scenes", "class_assets_1_1_scripts_1_1_u_i_1_1_menus_1_1_scenes.html", null ],
    [ "ScriptableObject", null, [
      [ "Assets.Scripts.Items.Item", "class_assets_1_1_scripts_1_1_items_1_1_item.html", [
        [ "Assets.Scripts.Items.Currency", "class_assets_1_1_scripts_1_1_items_1_1_currency.html", null ],
        [ "Assets.Scripts.Items.Gear", "class_assets_1_1_scripts_1_1_items_1_1_gear.html", [
          [ "Assets.Scripts.Items.Armor", "class_assets_1_1_scripts_1_1_items_1_1_armor.html", null ],
          [ "Assets.Scripts.Items.Weapon", "class_assets_1_1_scripts_1_1_items_1_1_weapon.html", null ]
        ] ],
        [ "Assets.Scripts.Items.UseableItem", "class_assets_1_1_scripts_1_1_items_1_1_useable_item.html", [
          [ "Assets.Scripts.Items.Potion", "class_assets_1_1_scripts_1_1_items_1_1_potion.html", null ]
        ] ]
      ] ]
    ] ],
    [ "Singleton", null, [
      [ "Assets.Scripts.Abilities.AbilityController", "class_assets_1_1_scripts_1_1_abilities_1_1_ability_controller.html", null ],
      [ "Assets.Scripts.Audio.AudioManager", "class_assets_1_1_scripts_1_1_audio_1_1_audio_manager.html", null ],
      [ "Assets.Scripts.Enemies.LootController", "class_assets_1_1_scripts_1_1_enemies_1_1_loot_controller.html", null ],
      [ "Assets.Scripts.Items.GearGenerator", "class_assets_1_1_scripts_1_1_items_1_1_gear_generator.html", null ],
      [ "Assets.Scripts.LevelGeneration.LevelController", "class_assets_1_1_scripts_1_1_level_generation_1_1_level_controller.html", null ],
      [ "Assets.Scripts.Player.PlayerStats", "class_assets_1_1_scripts_1_1_player_1_1_player_stats.html", null ],
      [ "Assets.Scripts.Player.PlayerUnit", "class_assets_1_1_scripts_1_1_player_1_1_player_unit.html", null ],
      [ "Assets.Scripts.UI.Inventory.InventoryManager", "class_assets_1_1_scripts_1_1_u_i_1_1_inventory_1_1_inventory_manager.html", null ],
      [ "Assets.Scripts.UI.Menus.InventoryMenu", "class_assets_1_1_scripts_1_1_u_i_1_1_menus_1_1_inventory_menu.html", null ],
      [ "Assets.Scripts.UI.Menus.StatDisplay", "class_assets_1_1_scripts_1_1_u_i_1_1_menus_1_1_stat_display.html", null ],
      [ "Assets.Scripts.UI.Resources.HealthBar", "class_assets_1_1_scripts_1_1_u_i_1_1_resources_1_1_health_bar.html", null ],
      [ "Assets.Scripts.UI.Resources.ManaBar", "class_assets_1_1_scripts_1_1_u_i_1_1_resources_1_1_mana_bar.html", null ],
      [ "Assets.Scripts.UI.Tooltip.Tooltip", "class_assets_1_1_scripts_1_1_u_i_1_1_tooltip_1_1_tooltip.html", null ],
      [ "Assets.Scripts.UI.Tooltip.TooltipEnabler", "class_assets_1_1_scripts_1_1_u_i_1_1_tooltip_1_1_tooltip_enabler.html", null ],
      [ "Assets.Scripts.UI.UIController", "class_assets_1_1_scripts_1_1_u_i_1_1_u_i_controller.html", null ],
      [ "Assets.Scripts.Utility.GameManager", "class_assets_1_1_scripts_1_1_utility_1_1_game_manager.html", null ]
    ] ],
    [ "Assets.Scripts.Audio.Sound", "class_assets_1_1_scripts_1_1_audio_1_1_sound.html", null ],
    [ "Assets.Scripts.UI.Menus.StatDisplayInfos", "class_assets_1_1_scripts_1_1_u_i_1_1_menus_1_1_stat_display_infos.html", null ],
    [ "Assets.Scripts.UI.Tooltip.TooltipInfos", "class_assets_1_1_scripts_1_1_u_i_1_1_tooltip_1_1_tooltip_infos.html", null ],
    [ "Assets.Scripts.Utility.UtilityMethods", "class_assets_1_1_scripts_1_1_utility_1_1_utility_methods.html", null ]
];