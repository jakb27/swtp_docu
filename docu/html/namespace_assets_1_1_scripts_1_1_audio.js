var namespace_assets_1_1_scripts_1_1_audio =
[
    [ "AudioManager", "class_assets_1_1_scripts_1_1_audio_1_1_audio_manager.html", "class_assets_1_1_scripts_1_1_audio_1_1_audio_manager" ],
    [ "Sound", "class_assets_1_1_scripts_1_1_audio_1_1_sound.html", "class_assets_1_1_scripts_1_1_audio_1_1_sound" ],
    [ "SFXAudio", "namespace_assets_1_1_scripts_1_1_audio.html#a0630ae3c21620547a6ec2b65ab425381", [
      [ "Jump", "namespace_assets_1_1_scripts_1_1_audio.html#a0630ae3c21620547a6ec2b65ab425381a101f693f72287a2819a364f64ca1c0ed", null ],
      [ "DoubleJump", "namespace_assets_1_1_scripts_1_1_audio.html#a0630ae3c21620547a6ec2b65ab425381a6bd5db223a35934e784d0b75a6d0d44e", null ],
      [ "Attack", "namespace_assets_1_1_scripts_1_1_audio.html#a0630ae3c21620547a6ec2b65ab425381adcfafcb4323b102c7e204555d313ba0a", null ],
      [ "PlayerTakeDamage", "namespace_assets_1_1_scripts_1_1_audio.html#a0630ae3c21620547a6ec2b65ab425381ae2a0e8096e149c208a2a99ef2a10e507", null ],
      [ "EnemyTakeDamage", "namespace_assets_1_1_scripts_1_1_audio.html#a0630ae3c21620547a6ec2b65ab425381a98f732622abcbe7d1dc198f46133da9c", null ],
      [ "UsePotion", "namespace_assets_1_1_scripts_1_1_audio.html#a0630ae3c21620547a6ec2b65ab425381a88d37289f9ba2c52962be54639c81c9c", null ],
      [ "DashMove", "namespace_assets_1_1_scripts_1_1_audio.html#a0630ae3c21620547a6ec2b65ab425381a292d1b194c1ee39f5c01bfe5aac4004f", null ],
      [ "HealAbility", "namespace_assets_1_1_scripts_1_1_audio.html#a0630ae3c21620547a6ec2b65ab425381a31fa06bf2c69980dadf6b843c3ef7500", null ],
      [ "SpeedAbility", "namespace_assets_1_1_scripts_1_1_audio.html#a0630ae3c21620547a6ec2b65ab425381a94cd6bdae86f6c1352d75b1cf2b4e4e9", null ],
      [ "PickupItem", "namespace_assets_1_1_scripts_1_1_audio.html#a0630ae3c21620547a6ec2b65ab425381aaaf525061dbd4d47d0efab1cfd2fcba0", null ],
      [ "SortInventory", "namespace_assets_1_1_scripts_1_1_audio.html#a0630ae3c21620547a6ec2b65ab425381a87c3069da656f388b66ed145cf775247", null ],
      [ "DeleteItem", "namespace_assets_1_1_scripts_1_1_audio.html#a0630ae3c21620547a6ec2b65ab425381a5e89b729b700c19c79bca0bb1d93421a", null ],
      [ "StartGame", "namespace_assets_1_1_scripts_1_1_audio.html#a0630ae3c21620547a6ec2b65ab425381a8a7b88cd602849e993a8ba3cdef39462", null ]
    ] ],
    [ "Soundtrack", "namespace_assets_1_1_scripts_1_1_audio.html#aacad2231be9c81e1f0f542906b823c23", [
      [ "MenuTheme", "namespace_assets_1_1_scripts_1_1_audio.html#aacad2231be9c81e1f0f542906b823c23a29d0d371b55cf4b44d2ae06beedff7e5", null ],
      [ "IngameTheme", "namespace_assets_1_1_scripts_1_1_audio.html#aacad2231be9c81e1f0f542906b823c23a63c8143fdba5023cc0f3be549354911c", null ]
    ] ]
];