var indexSectionsWithContent =
{
  0: "_abcdefghijklmnopqrstuvw",
  1: "abcdefghijlmprstuw",
  2: "a",
  3: "abcdefghijlmprstuw",
  4: "abcdefghijklopqrstuw",
  5: "_abcdefghiklmnprstuvw",
  6: "aegmprst",
  7: "abcdefghijlmnprstuw",
  8: "abcdefghiklmopstv",
  9: "d"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "enums",
  7: "enumvalues",
  8: "properties",
  9: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Enumerations",
  7: "Enumerator",
  8: "Properties",
  9: "Pages"
};

