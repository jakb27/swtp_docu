var class_assets_1_1_scripts_1_1_items_1_1_gear =
[
    [ "AddRawBonusesToPlayer", "class_assets_1_1_scripts_1_1_items_1_1_gear.html#abf5c5578338d036197ab6760583be62e", null ],
    [ "RemoveRawBonusesFromPlayer", "class_assets_1_1_scripts_1_1_items_1_1_gear.html#ab915e8d72018327b9893bd7d860df545", null ],
    [ "agilityBonus", "class_assets_1_1_scripts_1_1_items_1_1_gear.html#a08d8785972fc976cd1d2897443617880", null ],
    [ "gearRarity", "class_assets_1_1_scripts_1_1_items_1_1_gear.html#a72c0bdff475f4c326580e4790af8ce0c", null ],
    [ "gearSlot", "class_assets_1_1_scripts_1_1_items_1_1_gear.html#a6e04930043216cede76926e253b116db", null ],
    [ "intellectBonus", "class_assets_1_1_scripts_1_1_items_1_1_gear.html#aa3c02f083cfce30ee86fa55931f65c61", null ],
    [ "movementSpeedBonus", "class_assets_1_1_scripts_1_1_items_1_1_gear.html#a652e5fd4c4fef4737dd5b4948591dd02", null ],
    [ "primaryAttributeType", "class_assets_1_1_scripts_1_1_items_1_1_gear.html#ab8395e27f03db3e02a5e8dc766b3cad8", null ],
    [ "staminaBonus", "class_assets_1_1_scripts_1_1_items_1_1_gear.html#a18a7ef8ca4cc3338f4e77c6e9d582819", null ],
    [ "strengthBonus", "class_assets_1_1_scripts_1_1_items_1_1_gear.html#a614e9078a053983908ceee2672fc0a53", null ],
    [ "AgilityBonus", "class_assets_1_1_scripts_1_1_items_1_1_gear.html#a99cde3b19bf96508e5d121bc0ba4137d", null ],
    [ "GearRarity", "class_assets_1_1_scripts_1_1_items_1_1_gear.html#a87df107b986f3ddbf40fe8b832505f3d", null ],
    [ "GearSlot", "class_assets_1_1_scripts_1_1_items_1_1_gear.html#af5f56f798351d8cbc529670732fd119b", null ],
    [ "IntellectBonus", "class_assets_1_1_scripts_1_1_items_1_1_gear.html#a7c67999c52eff51e0a0660c83874608e", null ],
    [ "MovementSpeed", "class_assets_1_1_scripts_1_1_items_1_1_gear.html#a29da028a9e681b1e1c69628d6dcbe406", null ],
    [ "PrimaryAttributeType", "class_assets_1_1_scripts_1_1_items_1_1_gear.html#a591ece5d14dc204e64218e80a9837652", null ],
    [ "StaminaBonus", "class_assets_1_1_scripts_1_1_items_1_1_gear.html#a8ea4dc57fafe3099b202c1c954974c5f", null ],
    [ "StrengthBonus", "class_assets_1_1_scripts_1_1_items_1_1_gear.html#a50cd3ecb33033a4119545172e3f20193", null ]
];