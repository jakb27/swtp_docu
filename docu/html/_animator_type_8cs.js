var _animator_type_8cs =
[
    [ "AnimatorType", "_animator_type_8cs.html#a218a4a5f67f971009379abb22bdc5e61", [
      [ "MeleeEnemy", "_animator_type_8cs.html#a218a4a5f67f971009379abb22bdc5e61a44ecc547fd24912b629fcc183eaa305b", null ],
      [ "ChargeEnemy", "_animator_type_8cs.html#a218a4a5f67f971009379abb22bdc5e61a03509e30988fce8eb14caad85e1ed4cf", null ],
      [ "RangedEnemy", "_animator_type_8cs.html#a218a4a5f67f971009379abb22bdc5e61af8f2e76141a00f37d9c1f73ac0608101", null ],
      [ "FinalBoss", "_animator_type_8cs.html#a218a4a5f67f971009379abb22bdc5e61aabd44872ccc3619bf5000d27ab076611", null ]
    ] ]
];