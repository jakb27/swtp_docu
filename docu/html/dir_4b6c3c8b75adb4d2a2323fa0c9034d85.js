var dir_4b6c3c8b75adb4d2a2323fa0c9034d85 =
[
    [ "Attribute.cs", "_attribute_8cs.html", [
      [ "Attribute", "class_assets_1_1_scripts_1_1_attributes_1_1_attribute.html", "class_assets_1_1_scripts_1_1_attributes_1_1_attribute" ]
    ] ],
    [ "AttributeConversionRates.cs", "_attribute_conversion_rates_8cs.html", "_attribute_conversion_rates_8cs" ],
    [ "BaseBonus.cs", "_base_bonus_8cs.html", [
      [ "BaseBonus", "class_assets_1_1_scripts_1_1_attributes_1_1_base_bonus.html", "class_assets_1_1_scripts_1_1_attributes_1_1_base_bonus" ]
    ] ],
    [ "FinalBonus.cs", "_final_bonus_8cs.html", [
      [ "FinalBonus", "class_assets_1_1_scripts_1_1_attributes_1_1_final_bonus.html", "class_assets_1_1_scripts_1_1_attributes_1_1_final_bonus" ]
    ] ],
    [ "GearPrimaryAttribute.cs", "_gear_primary_attribute_8cs.html", "_gear_primary_attribute_8cs" ],
    [ "PrimaryAttribute.cs", "_primary_attribute_8cs.html", [
      [ "PrimaryAttribute", "class_assets_1_1_scripts_1_1_attributes_1_1_primary_attribute.html", "class_assets_1_1_scripts_1_1_attributes_1_1_primary_attribute" ]
    ] ],
    [ "RawBonus.cs", "_raw_bonus_8cs.html", [
      [ "RawBonus", "class_assets_1_1_scripts_1_1_attributes_1_1_raw_bonus.html", "class_assets_1_1_scripts_1_1_attributes_1_1_raw_bonus" ]
    ] ],
    [ "SecondaryAttribute.cs", "_secondary_attribute_8cs.html", [
      [ "SecondaryAttribute", "class_assets_1_1_scripts_1_1_attributes_1_1_secondary_attribute.html", "class_assets_1_1_scripts_1_1_attributes_1_1_secondary_attribute" ]
    ] ]
];