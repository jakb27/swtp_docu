var _gear_primary_attribute_8cs =
[
    [ "PrimaryAttributeType", "_gear_primary_attribute_8cs.html#ab89e33e2d7533cb7894cafe8baf9abd2", [
      [ "DEFAULT", "_gear_primary_attribute_8cs.html#ab89e33e2d7533cb7894cafe8baf9abd2a5b39c8b553c821e7cddc6da64b5bd2ee", null ],
      [ "STRENGTH", "_gear_primary_attribute_8cs.html#ab89e33e2d7533cb7894cafe8baf9abd2a3963049b60ddd6534b219d3767af0dfc", null ],
      [ "AGILITY", "_gear_primary_attribute_8cs.html#ab89e33e2d7533cb7894cafe8baf9abd2a3d3df0024a4bc24e1f5ada7545ab2e2d", null ],
      [ "INTELLECT", "_gear_primary_attribute_8cs.html#ab89e33e2d7533cb7894cafe8baf9abd2ad1e0b472d993de14239999c790edc9f4", null ],
      [ "STAMINA", "_gear_primary_attribute_8cs.html#ab89e33e2d7533cb7894cafe8baf9abd2a59e6d8b70719c87316406fd76bcf1030", null ]
    ] ]
];