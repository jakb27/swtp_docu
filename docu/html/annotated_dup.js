var annotated_dup =
[
    [ "Assets", "namespace_assets.html", [
      [ "Scripts", "namespace_assets_1_1_scripts.html", [
        [ "Abilities", "namespace_assets_1_1_scripts_1_1_abilities.html", [
          [ "AbilityController", "class_assets_1_1_scripts_1_1_abilities_1_1_ability_controller.html", "class_assets_1_1_scripts_1_1_abilities_1_1_ability_controller" ],
          [ "AbilityCooldowns", "class_assets_1_1_scripts_1_1_abilities_1_1_ability_cooldowns.html", "class_assets_1_1_scripts_1_1_abilities_1_1_ability_cooldowns" ],
          [ "AbilityCosts", "class_assets_1_1_scripts_1_1_abilities_1_1_ability_costs.html", "class_assets_1_1_scripts_1_1_abilities_1_1_ability_costs" ],
          [ "AbilityDash", "class_assets_1_1_scripts_1_1_abilities_1_1_ability_dash.html", "class_assets_1_1_scripts_1_1_abilities_1_1_ability_dash" ],
          [ "AbilityHeal", "class_assets_1_1_scripts_1_1_abilities_1_1_ability_heal.html", "class_assets_1_1_scripts_1_1_abilities_1_1_ability_heal" ],
          [ "AbilityShootBullet", "class_assets_1_1_scripts_1_1_abilities_1_1_ability_shoot_bullet.html", "class_assets_1_1_scripts_1_1_abilities_1_1_ability_shoot_bullet" ],
          [ "AbilitySpeedBuff", "class_assets_1_1_scripts_1_1_abilities_1_1_ability_speed_buff.html", "class_assets_1_1_scripts_1_1_abilities_1_1_ability_speed_buff" ],
          [ "AttackBullet", "class_assets_1_1_scripts_1_1_abilities_1_1_attack_bullet.html", "class_assets_1_1_scripts_1_1_abilities_1_1_attack_bullet" ],
          [ "BaseAbility", "class_assets_1_1_scripts_1_1_abilities_1_1_base_ability.html", "class_assets_1_1_scripts_1_1_abilities_1_1_base_ability" ]
        ] ],
        [ "Attributes", "namespace_assets_1_1_scripts_1_1_attributes.html", [
          [ "Attribute", "class_assets_1_1_scripts_1_1_attributes_1_1_attribute.html", "class_assets_1_1_scripts_1_1_attributes_1_1_attribute" ],
          [ "BaseBonus", "class_assets_1_1_scripts_1_1_attributes_1_1_base_bonus.html", "class_assets_1_1_scripts_1_1_attributes_1_1_base_bonus" ],
          [ "FinalBonus", "class_assets_1_1_scripts_1_1_attributes_1_1_final_bonus.html", "class_assets_1_1_scripts_1_1_attributes_1_1_final_bonus" ],
          [ "PrimaryAttribute", "class_assets_1_1_scripts_1_1_attributes_1_1_primary_attribute.html", "class_assets_1_1_scripts_1_1_attributes_1_1_primary_attribute" ],
          [ "RawBonus", "class_assets_1_1_scripts_1_1_attributes_1_1_raw_bonus.html", "class_assets_1_1_scripts_1_1_attributes_1_1_raw_bonus" ],
          [ "SecondaryAttribute", "class_assets_1_1_scripts_1_1_attributes_1_1_secondary_attribute.html", "class_assets_1_1_scripts_1_1_attributes_1_1_secondary_attribute" ]
        ] ],
        [ "Audio", "namespace_assets_1_1_scripts_1_1_audio.html", [
          [ "AudioManager", "class_assets_1_1_scripts_1_1_audio_1_1_audio_manager.html", "class_assets_1_1_scripts_1_1_audio_1_1_audio_manager" ],
          [ "Sound", "class_assets_1_1_scripts_1_1_audio_1_1_sound.html", "class_assets_1_1_scripts_1_1_audio_1_1_sound" ]
        ] ],
        [ "Enemies", "namespace_assets_1_1_scripts_1_1_enemies.html", [
          [ "BasicEnemy", "class_assets_1_1_scripts_1_1_enemies_1_1_basic_enemy.html", null ],
          [ "Enemy", "class_assets_1_1_scripts_1_1_enemies_1_1_enemy.html", "class_assets_1_1_scripts_1_1_enemies_1_1_enemy" ],
          [ "EnemyInfos", "class_assets_1_1_scripts_1_1_enemies_1_1_enemy_infos.html", "class_assets_1_1_scripts_1_1_enemies_1_1_enemy_infos" ],
          [ "EnemyMovement", "class_assets_1_1_scripts_1_1_enemies_1_1_enemy_movement.html", "class_assets_1_1_scripts_1_1_enemies_1_1_enemy_movement" ],
          [ "FieldOfView", "class_assets_1_1_scripts_1_1_enemies_1_1_field_of_view.html", "class_assets_1_1_scripts_1_1_enemies_1_1_field_of_view" ],
          [ "FinalBoss", "class_assets_1_1_scripts_1_1_enemies_1_1_final_boss.html", null ],
          [ "LootController", "class_assets_1_1_scripts_1_1_enemies_1_1_loot_controller.html", "class_assets_1_1_scripts_1_1_enemies_1_1_loot_controller" ]
        ] ],
        [ "Items", "namespace_assets_1_1_scripts_1_1_items.html", [
          [ "Armor", "class_assets_1_1_scripts_1_1_items_1_1_armor.html", "class_assets_1_1_scripts_1_1_items_1_1_armor" ],
          [ "Currency", "class_assets_1_1_scripts_1_1_items_1_1_currency.html", "class_assets_1_1_scripts_1_1_items_1_1_currency" ],
          [ "Gear", "class_assets_1_1_scripts_1_1_items_1_1_gear.html", "class_assets_1_1_scripts_1_1_items_1_1_gear" ],
          [ "GearGenerator", "class_assets_1_1_scripts_1_1_items_1_1_gear_generator.html", "class_assets_1_1_scripts_1_1_items_1_1_gear_generator" ],
          [ "GearInfos", "class_assets_1_1_scripts_1_1_items_1_1_gear_infos.html", "class_assets_1_1_scripts_1_1_items_1_1_gear_infos" ],
          [ "Item", "class_assets_1_1_scripts_1_1_items_1_1_item.html", "class_assets_1_1_scripts_1_1_items_1_1_item" ],
          [ "LootTable", "class_assets_1_1_scripts_1_1_items_1_1_loot_table.html", "class_assets_1_1_scripts_1_1_items_1_1_loot_table" ],
          [ "Potion", "class_assets_1_1_scripts_1_1_items_1_1_potion.html", "class_assets_1_1_scripts_1_1_items_1_1_potion" ],
          [ "UseableItem", "class_assets_1_1_scripts_1_1_items_1_1_useable_item.html", "class_assets_1_1_scripts_1_1_items_1_1_useable_item" ],
          [ "Weapon", "class_assets_1_1_scripts_1_1_items_1_1_weapon.html", "class_assets_1_1_scripts_1_1_items_1_1_weapon" ],
          [ "WorldItem", "class_assets_1_1_scripts_1_1_items_1_1_world_item.html", "class_assets_1_1_scripts_1_1_items_1_1_world_item" ]
        ] ],
        [ "LevelGeneration", "namespace_assets_1_1_scripts_1_1_level_generation.html", [
          [ "Level", "class_assets_1_1_scripts_1_1_level_generation_1_1_level.html", "class_assets_1_1_scripts_1_1_level_generation_1_1_level" ],
          [ "LevelController", "class_assets_1_1_scripts_1_1_level_generation_1_1_level_controller.html", "class_assets_1_1_scripts_1_1_level_generation_1_1_level_controller" ],
          [ "LevelRoom", "class_assets_1_1_scripts_1_1_level_generation_1_1_level_room.html", "class_assets_1_1_scripts_1_1_level_generation_1_1_level_room" ],
          [ "Room", "class_assets_1_1_scripts_1_1_level_generation_1_1_room.html", "class_assets_1_1_scripts_1_1_level_generation_1_1_room" ],
          [ "RoomInfos", "class_assets_1_1_scripts_1_1_level_generation_1_1_room_infos.html", "class_assets_1_1_scripts_1_1_level_generation_1_1_room_infos" ],
          [ "StartRoom", "class_assets_1_1_scripts_1_1_level_generation_1_1_start_room.html", "class_assets_1_1_scripts_1_1_level_generation_1_1_start_room" ]
        ] ],
        [ "Player", "namespace_assets_1_1_scripts_1_1_player.html", [
          [ "PlayerInfos", "class_assets_1_1_scripts_1_1_player_1_1_player_infos.html", "class_assets_1_1_scripts_1_1_player_1_1_player_infos" ],
          [ "PlayerStats", "class_assets_1_1_scripts_1_1_player_1_1_player_stats.html", "class_assets_1_1_scripts_1_1_player_1_1_player_stats" ],
          [ "PlayerUnit", "class_assets_1_1_scripts_1_1_player_1_1_player_unit.html", "class_assets_1_1_scripts_1_1_player_1_1_player_unit" ]
        ] ],
        [ "UI", "namespace_assets_1_1_scripts_1_1_u_i.html", [
          [ "Inventory", "namespace_assets_1_1_scripts_1_1_u_i_1_1_inventory.html", [
            [ "DragDropHandler", "class_assets_1_1_scripts_1_1_u_i_1_1_inventory_1_1_drag_drop_handler.html", "class_assets_1_1_scripts_1_1_u_i_1_1_inventory_1_1_drag_drop_handler" ],
            [ "Equipment", "class_assets_1_1_scripts_1_1_u_i_1_1_inventory_1_1_equipment.html", "class_assets_1_1_scripts_1_1_u_i_1_1_inventory_1_1_equipment" ],
            [ "EquipmentInfos", "class_assets_1_1_scripts_1_1_u_i_1_1_inventory_1_1_equipment_infos.html", "class_assets_1_1_scripts_1_1_u_i_1_1_inventory_1_1_equipment_infos" ],
            [ "EquipmentSlot", "class_assets_1_1_scripts_1_1_u_i_1_1_inventory_1_1_equipment_slot.html", "class_assets_1_1_scripts_1_1_u_i_1_1_inventory_1_1_equipment_slot" ],
            [ "InventoryButtonController", "class_assets_1_1_scripts_1_1_u_i_1_1_inventory_1_1_inventory_button_controller.html", "class_assets_1_1_scripts_1_1_u_i_1_1_inventory_1_1_inventory_button_controller" ],
            [ "InventoryManager", "class_assets_1_1_scripts_1_1_u_i_1_1_inventory_1_1_inventory_manager.html", "class_assets_1_1_scripts_1_1_u_i_1_1_inventory_1_1_inventory_manager" ],
            [ "InventorySlot", "class_assets_1_1_scripts_1_1_u_i_1_1_inventory_1_1_inventory_slot.html", "class_assets_1_1_scripts_1_1_u_i_1_1_inventory_1_1_inventory_slot" ]
          ] ],
          [ "Menus", "namespace_assets_1_1_scripts_1_1_u_i_1_1_menus.html", [
            [ "GameOverMenu", "class_assets_1_1_scripts_1_1_u_i_1_1_menus_1_1_game_over_menu.html", "class_assets_1_1_scripts_1_1_u_i_1_1_menus_1_1_game_over_menu" ],
            [ "GameWonMenu", "class_assets_1_1_scripts_1_1_u_i_1_1_menus_1_1_game_won_menu.html", "class_assets_1_1_scripts_1_1_u_i_1_1_menus_1_1_game_won_menu" ],
            [ "IngameMenu", "class_assets_1_1_scripts_1_1_u_i_1_1_menus_1_1_ingame_menu.html", "class_assets_1_1_scripts_1_1_u_i_1_1_menus_1_1_ingame_menu" ],
            [ "InventoryMenu", "class_assets_1_1_scripts_1_1_u_i_1_1_menus_1_1_inventory_menu.html", "class_assets_1_1_scripts_1_1_u_i_1_1_menus_1_1_inventory_menu" ],
            [ "MainMenu", "class_assets_1_1_scripts_1_1_u_i_1_1_menus_1_1_main_menu.html", "class_assets_1_1_scripts_1_1_u_i_1_1_menus_1_1_main_menu" ],
            [ "PauseMenu", "class_assets_1_1_scripts_1_1_u_i_1_1_menus_1_1_pause_menu.html", "class_assets_1_1_scripts_1_1_u_i_1_1_menus_1_1_pause_menu" ],
            [ "Scenes", "class_assets_1_1_scripts_1_1_u_i_1_1_menus_1_1_scenes.html", "class_assets_1_1_scripts_1_1_u_i_1_1_menus_1_1_scenes" ],
            [ "StatDisplay", "class_assets_1_1_scripts_1_1_u_i_1_1_menus_1_1_stat_display.html", "class_assets_1_1_scripts_1_1_u_i_1_1_menus_1_1_stat_display" ],
            [ "StatDisplayElement", "class_assets_1_1_scripts_1_1_u_i_1_1_menus_1_1_stat_display_element.html", "class_assets_1_1_scripts_1_1_u_i_1_1_menus_1_1_stat_display_element" ],
            [ "StatDisplayInfos", "class_assets_1_1_scripts_1_1_u_i_1_1_menus_1_1_stat_display_infos.html", "class_assets_1_1_scripts_1_1_u_i_1_1_menus_1_1_stat_display_infos" ]
          ] ],
          [ "Resources", "namespace_assets_1_1_scripts_1_1_u_i_1_1_resources.html", [
            [ "HealthBar", "class_assets_1_1_scripts_1_1_u_i_1_1_resources_1_1_health_bar.html", "class_assets_1_1_scripts_1_1_u_i_1_1_resources_1_1_health_bar" ],
            [ "ManaBar", "class_assets_1_1_scripts_1_1_u_i_1_1_resources_1_1_mana_bar.html", "class_assets_1_1_scripts_1_1_u_i_1_1_resources_1_1_mana_bar" ]
          ] ],
          [ "Tooltip", "namespace_assets_1_1_scripts_1_1_u_i_1_1_tooltip.html", [
            [ "Tooltip", "class_assets_1_1_scripts_1_1_u_i_1_1_tooltip_1_1_tooltip.html", "class_assets_1_1_scripts_1_1_u_i_1_1_tooltip_1_1_tooltip" ],
            [ "TooltipEnabler", "class_assets_1_1_scripts_1_1_u_i_1_1_tooltip_1_1_tooltip_enabler.html", "class_assets_1_1_scripts_1_1_u_i_1_1_tooltip_1_1_tooltip_enabler" ],
            [ "TooltipInfos", "class_assets_1_1_scripts_1_1_u_i_1_1_tooltip_1_1_tooltip_infos.html", "class_assets_1_1_scripts_1_1_u_i_1_1_tooltip_1_1_tooltip_infos" ]
          ] ],
          [ "UIController", "class_assets_1_1_scripts_1_1_u_i_1_1_u_i_controller.html", "class_assets_1_1_scripts_1_1_u_i_1_1_u_i_controller" ]
        ] ],
        [ "Utility", "namespace_assets_1_1_scripts_1_1_utility.html", [
          [ "JsonSerializer", "namespace_assets_1_1_scripts_1_1_utility_1_1_json_serializer.html", [
            [ "DataSerializer", "class_assets_1_1_scripts_1_1_utility_1_1_json_serializer_1_1_data_serializer.html", "class_assets_1_1_scripts_1_1_utility_1_1_json_serializer_1_1_data_serializer" ],
            [ "GameDifficulty", "class_assets_1_1_scripts_1_1_utility_1_1_json_serializer_1_1_game_difficulty.html", "class_assets_1_1_scripts_1_1_utility_1_1_json_serializer_1_1_game_difficulty" ],
            [ "ISerializedObject", "interface_assets_1_1_scripts_1_1_utility_1_1_json_serializer_1_1_i_serialized_object.html", null ],
            [ "ItemNames", "class_assets_1_1_scripts_1_1_utility_1_1_json_serializer_1_1_item_names.html", "class_assets_1_1_scripts_1_1_utility_1_1_json_serializer_1_1_item_names" ],
            [ "JsonFileNames", "class_assets_1_1_scripts_1_1_utility_1_1_json_serializer_1_1_json_file_names.html", "class_assets_1_1_scripts_1_1_utility_1_1_json_serializer_1_1_json_file_names" ],
            [ "StatDisplayInitialization", "class_assets_1_1_scripts_1_1_utility_1_1_json_serializer_1_1_stat_display_initialization.html", "class_assets_1_1_scripts_1_1_utility_1_1_json_serializer_1_1_stat_display_initialization" ]
          ] ],
          [ "GameManager", "class_assets_1_1_scripts_1_1_utility_1_1_game_manager.html", "class_assets_1_1_scripts_1_1_utility_1_1_game_manager" ],
          [ "Globals", "class_assets_1_1_scripts_1_1_utility_1_1_globals.html", "class_assets_1_1_scripts_1_1_utility_1_1_globals" ],
          [ "LoggingService", "class_assets_1_1_scripts_1_1_utility_1_1_logging_service.html", "class_assets_1_1_scripts_1_1_utility_1_1_logging_service" ],
          [ "UtilityMethods", "class_assets_1_1_scripts_1_1_utility_1_1_utility_methods.html", "class_assets_1_1_scripts_1_1_utility_1_1_utility_methods" ]
        ] ]
      ] ]
    ] ]
];