var namespace_assets_1_1_scripts_1_1_attributes =
[
    [ "Attribute", "class_assets_1_1_scripts_1_1_attributes_1_1_attribute.html", "class_assets_1_1_scripts_1_1_attributes_1_1_attribute" ],
    [ "BaseBonus", "class_assets_1_1_scripts_1_1_attributes_1_1_base_bonus.html", "class_assets_1_1_scripts_1_1_attributes_1_1_base_bonus" ],
    [ "FinalBonus", "class_assets_1_1_scripts_1_1_attributes_1_1_final_bonus.html", "class_assets_1_1_scripts_1_1_attributes_1_1_final_bonus" ],
    [ "PrimaryAttribute", "class_assets_1_1_scripts_1_1_attributes_1_1_primary_attribute.html", "class_assets_1_1_scripts_1_1_attributes_1_1_primary_attribute" ],
    [ "RawBonus", "class_assets_1_1_scripts_1_1_attributes_1_1_raw_bonus.html", "class_assets_1_1_scripts_1_1_attributes_1_1_raw_bonus" ],
    [ "SecondaryAttribute", "class_assets_1_1_scripts_1_1_attributes_1_1_secondary_attribute.html", "class_assets_1_1_scripts_1_1_attributes_1_1_secondary_attribute" ],
    [ "AttributeConversionRates", "namespace_assets_1_1_scripts_1_1_attributes.html#aec4a1829ecde34a782a8e78a967d1327", [
      [ "StrengthToAttackPower", "namespace_assets_1_1_scripts_1_1_attributes.html#aec4a1829ecde34a782a8e78a967d1327abab4e096adbb2a2f8cee5ad5b9d3ef20", null ],
      [ "AgilityToAttackSpeed", "namespace_assets_1_1_scripts_1_1_attributes.html#aec4a1829ecde34a782a8e78a967d1327a7d907d221899e10648c7812724302182", null ],
      [ "AgilityToMovementSpeed", "namespace_assets_1_1_scripts_1_1_attributes.html#aec4a1829ecde34a782a8e78a967d1327afea01cd5bc92091fc2dfd376f6e94e75", null ],
      [ "IntellectToMana", "namespace_assets_1_1_scripts_1_1_attributes.html#aec4a1829ecde34a782a8e78a967d1327a592afa82f217826dadecd0f61463af26", null ],
      [ "IntellectToSpellPower", "namespace_assets_1_1_scripts_1_1_attributes.html#aec4a1829ecde34a782a8e78a967d1327a44f03b910a732165536e1c54f7722d76", null ],
      [ "StaminaToHealth", "namespace_assets_1_1_scripts_1_1_attributes.html#aec4a1829ecde34a782a8e78a967d1327aab812ed2ae22e75087a737a703a41743", null ],
      [ "StaminaToArmor", "namespace_assets_1_1_scripts_1_1_attributes.html#aec4a1829ecde34a782a8e78a967d1327aad36c2f4ac72358d14258e7b930f16d1", null ]
    ] ],
    [ "PrimaryAttributeType", "namespace_assets_1_1_scripts_1_1_attributes.html#ab89e33e2d7533cb7894cafe8baf9abd2", [
      [ "DEFAULT", "namespace_assets_1_1_scripts_1_1_attributes.html#ab89e33e2d7533cb7894cafe8baf9abd2a5b39c8b553c821e7cddc6da64b5bd2ee", null ],
      [ "STRENGTH", "namespace_assets_1_1_scripts_1_1_attributes.html#ab89e33e2d7533cb7894cafe8baf9abd2a3963049b60ddd6534b219d3767af0dfc", null ],
      [ "AGILITY", "namespace_assets_1_1_scripts_1_1_attributes.html#ab89e33e2d7533cb7894cafe8baf9abd2a3d3df0024a4bc24e1f5ada7545ab2e2d", null ],
      [ "INTELLECT", "namespace_assets_1_1_scripts_1_1_attributes.html#ab89e33e2d7533cb7894cafe8baf9abd2ad1e0b472d993de14239999c790edc9f4", null ],
      [ "STAMINA", "namespace_assets_1_1_scripts_1_1_attributes.html#ab89e33e2d7533cb7894cafe8baf9abd2a59e6d8b70719c87316406fd76bcf1030", null ]
    ] ]
];